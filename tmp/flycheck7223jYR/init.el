(load "~/.emacs.d/emacs-rc-cedet-devel.el")
(load "~/.emacs.d/misc-settings.el")


;; ,----
;; |
;; |
;; | PROGRAMMING SPECIFIC MODES AND CONFIGURATIONS
;; |
;; |
;; `----

;;auto-indent when enter
(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (and (not current-prefix-arg)
                (member major-mode '(emacs-lisp-mode lisp-mode
                                                     clojure-mode    scheme-mode
                                                     haskell-mode    ruby-mode
                                                     rspec-mode      python-mode
                                                     c-mode          c++-mode
                                                     objc-mode       latex-mode
                                                     plain-tex-mode))
                (let ((mark-even-if-inactive transient-mark-mode))
                  (indent-region (region-beginning) (region-end) nil))))))
(define-key global-map (kbd "RET") 'newline-and-indent)

;;gas-mode
(add-to-list 'load-path "~/.emacs.d/gas-mode")
(add-to-list 'load-path "~/.emacs.d/myplan")

;;asmMODE
(autoload 'asm86-mode "~/.emacs.d/asm86-mode")
(setq auto-mode-alist
      (append '(("\\.asm\\'" . asm86-mode) ("\\.inc\\'" . asm86-mode)) auto-mode-alist))
(add-hook 'asm86-mode-hook 'turn-on-font-lock)
(add-hook 'asm86-mode-hook
          '(lambda ()
             (define-key asm86-mode-map "\r" 'reindent-then-newline-and-indent)))

;;flex-mode
(load "~/.emacs.d/flex-mode")
(setq auto-mode-alist (cons '("\\.flex$" . flex-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.y$" . c++-mode) auto-mode-alist))

;;slime
(setq inferior-lisp-program "/usr/bin/sbcl") ; your Lisp system
(add-to-list 'load-path "~/.emacs.d/slime/")  ; your SLIME directory
(add-to-list 'load-path "~/.emacs.d/slime/contrib/")  ; your SLIME directory

(eval-after-load "slime"
  '(progn
     (setq slime-lisp-implementations
           '((sbcl ("/usr/bin/sbcl"))
             (ecl ("/usr/bin/ecl"))
             (clisp ("/usr/bin/clisp"))))
     (slime-setup '(
                    slime-asdf
                    slime-autodoc
                    slime-editing-commands
                    slime-fancy-inspector
                    slime-fontifying-fu
                    slime-fuzzy
                    slime-indentation
                                        ;slime-mdot-fu
                    slime-package-fu
                    slime-references
                    slime-repl
                    slime-sbcl-exts
                    slime-scratch
                    slime-xref-browser
                    ))
     (slime-autodoc-mode)
     (setq slime-complete-symbol*-fancy t)
     (setq slime-complete-symbol-function
           'slime-fuzzy-complete-symbol)))

(require 'slime)
(slime-setup)

;;par-edit
(load "~/.emacs.d/paredit")
(autoload 'paredit-mode "paredit"
  "Minor mode for pseudo-structurally editing Lisp code." t)
(add-hook 'slime-repl-mode-hook (lambda () (paredit-mode +1)))
(add-hook 'emacs-lisp-mode-hook       (lambda () (paredit-mode +1)))
(add-hook 'lisp-mode-hook             (lambda () (paredit-mode +1)))
(add-hook 'lisp-interaction-mode-hook (lambda () (paredit-mode +1)))
(add-hook 'scheme-mode-hook           (lambda () (paredit-mode +1)))

(defadvice paredit-mode (around disable-autopairs-around (arg))
  "Disable autopairs mode if paredit-mode is turned on"
  ad-do-it
  (if (null ad-return-value)
      (autopair-mode 1)
    (autopair-mode 0)))
(ad-activate 'paredit-mode)
;; ,----
;; |
;; | C/C++
;; |
;; `----

;;Switch between c/c++ source and header file
(defun dts-switch-between-header-and-source ()
  "Switch between a c/c++ header (.h) and its corresponding source (.c/.cpp)."
  (interactive)
  ;; grab the base of the current buffer's file name
  (setq bse (file-name-sans-extension buffer-file-name))
  ;; and the extension, converted to lowercase so we can
  ;; compare it to "h", "c", "cpp", etc
  (setq ext (downcase (file-name-extension buffer-file-name)))
  ;; This is like a c/c++ switch statement, except that the
  ;; conditions can be any true/false evaluated statement
  (cond
   ;; first condition - the extension is "h"
   ((equal ext "h")
    ;; first, look for bse.c
    (setq nfn (concat bse ".c"))
    (if (file-exists-p nfn)
        ;; if it exists, either switch to an already-open
        ;;  buffer containing that file, or open it in a new buffer
        (find-file nfn)
      ;; this is the "else" part - note that if it is more than
      ;;  one line, it needs to be wrapped in a (progn )
      (progn
        ;; look for a bse.cpp
        (setq nfn (concat bse ".cpp"))
        ;; likewise
        (find-file nfn))))
   ;; second condition - the extension is "c" or "cpp"
   ((or (equal ext "cpp") (equal ext "c"))
    ;; look for a corresponding bse.h
    (setq nfn (concat bse ".h"))
    (find-file nfn))))

(global-set-key (kbd "C-c s") 'dts-switch-between-header-and-source)

;; flymake
(add-to-list 'load-path "~/.emacs.d/flymake")
(load "~/.emacs.d/fringe-helper")
;; (load "~/.emacs.d/flymake-extension")
;; (load "~/.emacs.d/indicators")

(require 'flymake)
;;(require 'flymake-extension)
(require 'flymake-cursor)
(require 'fringe-helper)
(require 'flymake-shell)
;; (require 'indicator)

(require 'rfringe)
(add-hook 'sh-mode-hook 'flymake-shell-load)

(setq flymake-max-parallel-syntax-checks nil)
(setq flymake-run-in-place nil)
(setq flymake-number-of-errors-to-display nil)
(setq temporary-file-directory "~/.emacs.d/tmp/")

(defvar flymake-fringe-overlays nil)
(make-variable-buffer-local 'flymake-fringe-overlays)

(defadvice flymake-make-overlay (after add-to-fringe first
                                       (beg end tooltip-text face mouse-face)
                                       activate compile)
  (push (fringe-helper-insert-region
         beg end
         (fringe-lib-load (if (eq face 'flymake-errline)
                              fringe-lib-exclamation-mark
                            fringe-lib-question-mark))
         'left-fringe 'font-lock-warning-face)
        flymake-fringe-overlays))

(defadvice flymake-delete-own-overlays (after remove-from-fringe activate
                                              compile)
  (mapc 'fringe-helper-remove flymake-fringe-overlays)
  (setq flymake-fringe-overlays nil))

;; ;;cedet
;; (load-file "~/.emacs.d/emacs-rc-cedet.el")

;; Define function
(defun flymake-cc-init ()
  (let* (;; Create temp file which is copy of current file
         (temp-file   (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
         ;; Get relative path of temp file from current directory
         (local-file  (file-relative-name
                       temp-file
                       (file-name-directory buffer-file-name))))

    ;; Construct compile command which is defined list.
    ;; First element is program name, "g++" in this case.
    ;; Second element is list of options.
    ;; So this means "g++ -Wall -Wextra -fsyntax-only tempfile-path"
    (list "g++" (list "-std=c++11" "-Wall" "-Wextra" "-fsyntax-only" local-file))))

;; Enable above flymake setting for C++ files(suffix is '.cpp')
(push '("\\.cpp$" flymake-cc-init) flymake-allowed-file-name-masks)

;;set c++ style
(add-hook 'c++-mode-hook
          '(lambda ( )
             (c-set-style "Stroustrup")
             (c-toggle-auto-state)))

;;cc-mode linux style
(setq c-default-style "linux"
      c-basic-offset 4)
;;clang-complete
(load "~/.emacs.d/auto-complete-clang-config")
(require 'auto-complete-clang)

(defun my-ac-cc-mode-setup ()
  (setq ac-sources (append '(ac-source-clang ac-source-yasnippet) ac-sources)))
(add-hook 'c-mode-common-hook 'my-ac-cc-mode-setup)

;; Enable flymake-mode for C++ files.
(add-hook 'c++-mode-hook 'flymake-mode)
;; ,----
;; |
;; | Erlang-mode
;; |
;; `----
(setq load-path (cons  "/usr/local/lib/erlang/lib/tools-2.6.12/emacs/"
                       load-path))
(setq erlang-root-dir "/usr/local/lib/erlang")
(setq exec-path (cons "/usr/local/lib/erlang/bin" exec-path))
(require 'erlang-start)
;;flymake-erlang
(require 'erlang-flymake)
(require 'flymake)
(defun flymake-erlang-init ()
  (let* ((temp-file (flymake-init-create-temp-buffer-copy
                     'flymake-create-temp-inplace))
         (local-file (file-relative-name temp-file
                                         (file-name-directory buffer-file-name))))
    (list "~/.emacs.d/check_erlang.erl" (list local-file))))

(add-to-list 'flymake-allowed-file-name-masks '("\\.erl\\'" flymake-erlang-init))
(add-to-list 'flymake-allowed-file-name-masks '("\\.hrl\\'" flymake-erlang-init))
;; ;; Distel
(push "~/.emacs.d/distel/elisp/" load-path)
(require 'distel)
(distel-setup)

;; ,----
;; |
;; | Ada-mode
;; |
;; `----
(add-to-list 'load-path "~/.emacs.d/ada-mode")
(require 'ada-mode)

;; Flymake for Ada
(require 'flymake)
(defun flymake-ada-init ()
  (flymake-simple-make-init-impl
   'flymake-create-temp-with-folder-structure nil nil
   buffer-file-name
   'flymake-get-ada-cmdline))

(defun flymake-get-ada-cmdline (source base-dir)
  `("gnatmake" ("-gnatc" "-gnatwa" ,(concat "-I" (expand-file-name base-dir)) ,source)))

(push '(".+\\.adb$" flymake-ada-init) flymake-allowed-file-name-masks)
(push '(".+\\.ads$" flymake-ada-init) flymake-allowed-file-name-masks)

(push '("\\([^:]*\\):\\([0-9]+\\):[0-9]+: \\(.*\\)"
        1 2 nil 3)
      flymake-err-line-patterns)
;; ,----
;; |
;; | cflow
;; | - EXTERNAL REQUIREMENT: cflow
;; |
;; `----
(load "~/.emacs.d/cflow-mode")
(autoload 'cflow-mode "cflow-mode")
(setq auto-mode-alist (append auto-mode-alist
                              '(("\\.cflow$" . cflow-mode))))
;; ;;scala
;; ;; load the ensime lisp code...
;; (add-to-list 'load-path "~/.emacs.d/ensime/src/main/elisp/")
;; (require 'ensime)

;; This step causes the ensime-mode to be started whenever
;; scala-mode is started for a buffer. You may have to customize this step
;; if you're not using the standard scala mode.
;; (add-hook 'scala-mode-hook 'ensime-scala-mode-hook)

;; ,----
;; |
;; | Prolog-mode
;; |
;; `----
(load "~/.emacs.d/prolog")
(setq prolog-program-name "~/BProlog/bp")
(autoload 'run-prolog "prolog" "Start a Prolog sub-process." t)
(autoload 'prolog-mode "prolog" "Major mode for editing Prolog programs." t)
(autoload 'mercury-mode "prolog" "Major mode for editing Mercury programs." t)
(setq prolog-system 'swi)  ; optional, the system you are using;
                                        ; see `prolog-system' below for possible values
(setq auto-mode-alist (append '(("\\.pl$" . prolog-mode)
                                ("\\.m$" . mercury-mode)) auto-mode-alist))

(setq prolog-indent-width 4
      ;;             prolog-electric-dot-flag t
      ;;             prolog-electric-dash-flag t
      prolog-electric-colon-flag t)
(setq prolog-system 'swi
      prolog-program-switches '((swi ("-G128M" "-T128M" "-L128M" "-O"))
                                (t nil))
      prolog-electric-if-then-else-flag t)


;;prolog-flymake
(add-hook 'prolog-mode-hook
          (lambda ()
            (require 'flymake)
            (make-local-variable 'flymake-allowed-file-name-masks)
            (make-local-variable 'flymake-err-line-patterns)
            (setq flymake-err-line-patterns
                  '(("ERROR: (?\\(.*?\\):\\([0-9]+\\)" 1 2)
                    ("Warning: (\\(.*\\):\\([0-9]+\\)" 1 2)))
            (setq flymake-allowed-file-name-masks
                  '(("\\.pl\\'" flymake-prolog-init)))
            (flymake-mode 1)))

(defun flymake-prolog-init ()
  (let* ((temp-file   (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
         (local-file  (file-relative-name
                       temp-file
                       (file-name-directory buffer-file-name))))
    (list "prolog" (list "-q" "-t" "halt" "-s " local-file))))


;;ruby-mode
(defun ruby-mode-hook ()
  (autoload 'ruby-mode "ruby-mode" nil t)
  (add-to-list 'auto-mode-alist '("Capfile" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Gemfile" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Rakefile" . ruby-mode))
  (add-to-list 'auto-mode-alist '("\\.rake\\'" . ruby-mode))
  (add-to-list 'auto-mode-alist '("\\.rb\\'" . ruby-mode))
  (add-to-list 'auto-mode-alist '("\\.ru\\'" . ruby-mode))
  (add-hook 'ruby-mode-hook '(lambda ()
                               (setq ruby-deep-arglist t)
                               (setq ruby-deep-indent-paren nil)
                               (setq c-tab-always-indent nil)
                               (require 'inf-ruby)
                               (require 'ruby-compilation))))
(defun rhtml-mode-hook ()
  (autoload 'rhtml-mode "rhtml-mode" nil t)
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . rhtml-mode))
  (add-to-list 'auto-mode-alist '("\\.rjs\\'" . rhtml-mode))
  (add-hook 'rhtml-mode '(lambda ()
                           (define-key rhtml-mode-map (kbd "M-s") 'save-buffer))))
(defun yaml-mode-hook ()
  (autoload 'yaml-mode "yaml-mode" nil t)
  (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
  (add-to-list 'auto-mode-alist '("\\.yaml$" . yaml-mode)))
(defun css-mode-hook ()
  (autoload 'css-mode "css-mode" nil t)
  (add-hook 'css-mode-hook '(lambda ()
                              (setq css-indent-level 2)
                              (setq css-indent-offset 2))))

(eval-after-load 'ruby-mode
  '(progn
     (define-key ruby-mode-map (kbd "RET") 'reindent-then-newline-and-indent)
     (define-key ruby-mode-map (kbd "TAB") 'indent-for-tab-command)))

;;octave
(autoload 'octave-mode "octave-mode" nil t)
(setq auto-mode-alist
      (cons '("\\.m$" . octave-mode) auto-mode-alist))
(add-hook 'octave-mode-hook
          (lambda ()
            (abbrev-mode 1)
            (auto-fill-mode 1)
            (if (eq window-system 'x)
                (font-lock-mode 1))))



;;emacs-ctable
(require 'ctable)

;;emacs deferred
;; (add-to-list 'load-path "~/.emacs.d/emacs-deferred")

;; ;;emacs-epc
;; (add-to-list 'load-path "~/.emacs.d/emacs-epc")
;; (require 'epc)
;; ;;jedi-mode
;; (add-to-list 'load-path "~/.emacs.d/emacs-jedi")
;; (autoload 'jedi:setup "jedi" nil t)
;; (add-hook 'python-mode-hook 'auto-complete-mode)
;; (add-hook 'python-mode-hook 'jedi:setup)
;;                                         ;(add-hook 'python-mode-hook 'electric-pair-mode)
;; (setq jedi:setup-keys t)                      ; optional
;; (setq jedi:complete-on-dot t)                 ; optional
;; (eval-after-load "python"
;;   '(define-key python-mode-map "\C-cx" 'jedi-direx:pop-to-buffer))
;;fly-check - installed by el-get
                                        ;(load "~/.emacs.d/flycheck/flycheck")

                                        ;(require 'flycheck)
                                        ;(add-hook 'after-init-hook #'global-flycheck-mode)
;;smartparens
                                        ;(load "~/.emacs.d/smartpar-conf")

;;flycheck-color-mode

                                        ;(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode)

;;(eval-after-load "python"
;;  '(define-key python-mode-map "C-m" 'newline-and-indent))
(eval-after-load "python"
  '(define-key python-mode-map "M-e" 'forward-sentence))


;; (add-to-list 'load-path "~/.emacs.d/helm")
;; (require 'helm-config)
;; (helm-mode 1)
;;julia-mode
(load "~/.emacs.d/julia-mode")
(require 'julia-mode)
;;org-mode
(add-to-list 'load-path "~/.emacs.d/orgmode")
(add-to-list 'load-path "~/.emacs.d/orgmode/contrib/lisp" t)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
;; (global-font-lock-mode 1)  Not needed in recent emacsen
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
(setq org-agenda-files (list "~/org/test.org"
                             "~/org/test2.org"
                             "~/org/test3.org"))

;;emacs-eclim
(add-to-list 'load-path "~/.emacs.d/emacs-eclim/")
(require 'eclim)
                                        ;(global-eclim-mode)
(require 'eclimd)

(setq eclim-auto-save t
      eclim-executable "~/Programs/eclipse/eclim"
      eclimd-executable "~/Programs/eclipse/eclimd"
      eclimd-wait-for-process nil
      eclimd-default-workspace "~/src/Workspace"
      eclim-use-yasnippet nil
      help-at-pt-display-when-idle t
      help-at-pt-timer-delay 0.1)

(setq help-at-pt-display-when-idle t)
(setq help-at-pt-timer-delay 0.1)
(help-at-pt-set-timer)

;; regular auto-complete initialization

;;(require 'auto-complete-config)
;;(ac-config-default)

;; add the emacs-eclim source
(require 'ac-emacs-eclim-source)
(ac-emacs-eclim-config)

;; (require 'company)
;; (require 'company-emacs-eclim)
;; (company-emacs-eclim-setup)
;;(global-company-mode t)
;; (define-key ac-mode-map  [(backtab)] 'company-emacs-eclim)
;;jdee
;; (add-to-list 'load-path "~/.emacs.d/jdee-2.4.1/lisp/")
;; (autoload 'jde-mode "jde" "JDE mode" t)
;; (setq auto-mode-alist
;;       (append '(("\\.java\\'" . jde-mode)) auto-mode-alist))
;; (push 'jde-mode ac-modes)
;; (add-hook 'jde-mode-hook (lambda () (push 'ac-source-semantic ac-sources)))
;;autogenerated code for color-theme-sanityinc-tomorrow-night

;; (color-theme-sanityinc-tomorrow-eighties)
