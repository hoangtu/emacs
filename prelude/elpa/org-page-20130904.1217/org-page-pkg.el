(define-package "org-page" "20130904.1217" "a static site generator based on org mode [github]"
  '((ht "1.5")
    (mustache "0.22")
    (htmlize "1.47")
    (org "8.0")
    (dash "2.0.0")))

