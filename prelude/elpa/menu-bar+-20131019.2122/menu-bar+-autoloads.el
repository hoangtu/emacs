;;; menu-bar+-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (menu-bar-next-tag-other-window kill-this-buffer
;;;;;;  menu-bar-create-directory describe-menubar) "menu-bar+" "menu-bar+.el"
;;;;;;  (21115 56955 519133 439000))
;;; Generated autoloads from menu-bar+.el

(autoload 'describe-menubar "menu-bar+" "\
Explain the menu bar, in general terms.

\(fn)" t nil)

(autoload 'menu-bar-create-directory "menu-bar+" "\
Create a subdirectory of `default-directory' called DIRECTORY.

\(fn DIRECTORY)" t nil)

(autoload 'kill-this-buffer "menu-bar+" "\
Delete the current buffer and delete all of its windows.

\(fn)" t nil)

(autoload 'menu-bar-next-tag-other-window "menu-bar+" "\
Find the next definition of the tag already specified.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("menu-bar+-pkg.el") (21115 56955 671933
;;;;;;  831000))

;;;***

(provide 'menu-bar+-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; menu-bar+-autoloads.el ends here
