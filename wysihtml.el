;; Routines for "what you see is HTML".
;; copyright 2003 Junichi Uekawa.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.
;;
;; readme-debian.el is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with your Debian installation, in /usr/share/common-licenses/GPL
;; If not, write to the Free Software Foundation, 675 Mass Ave,
;; Cambridge, MA 02139, USA.

(defvar wysihtml-daemon-name "@LIBEXECDIR@/wysihtmldaemon" 
  "The default value for wysihtml daemon")
(defconst wysihtml-daemon-process-name "wysihtmldaemon-interactive")
(defvar wysihtml-daemon-buffer nil 
  "The buffer name for wysihtml daemon, this value should not be modified by 
user")
(defvar wysihtml-active-modes-alist '((html-mode . t))
  "*List of major modes that wysihtml is activated"
  )
(defconst wysihtml-version "@VERSION@"
  "The version number of wysihtml")
(defvar wysihtml-elserv-port 8089 "Elserv port used for wysihtml")
(defvar wysihtml-elserv-process nil "The elserv process identifier for the elserv process being used for wysihtml.")
(defvar wysihtml-current-buffer nil 
  "The buffer which is going to be displayed through elserv,
for preview.")
(defvar wysihtml-interactive-time 1
  "The number of seconds to wait before renew, slow machines may want
several seconds, but fast machines can have less, to make speed")


(require 'comint)
(require 'elserv)
(require 'mcharset)

(defun wysihtml-start-daemon ()
  "Start up daemon for wysihtml, and assign a buffer for the running process"
  (setq wysihtml-daemon-buffer 
	(make-comint wysihtml-daemon-process-name
		     wysihtml-daemon-name
		     nil "-s" (number-to-string wysihtml-interactive-time))))

(defun wysihtml-restart-daemon ()
  "Restart the daemon after reconfiguration"
  (interactive)
  (kill-buffer wysihtml-daemon-buffer)
  (wysihtml-start-daemon))


(defun wysihtml-start-elserv ()
  "Internal routine to start up elserv process.
wysihtml uses this process for displaying HTML through MOZILLA."
  (setq wysihtml-elserv-process (elserv-start wysihtml-elserv-port))
  (elserv-publish wysihtml-elserv-process
		  "/preview"
		  :function 'wysihtml-elserv-preview
		  :description "Preview of current HTML buffer"))


(defun wysihtml-elserv-preview (result path ppath request) 
  "View HTML from buffer.

RESULT is the resulting value
PATH is relative path from the published path
PPATH is the published path
REQUEST is the request data."
  (save-excursion
    (let* ((charset nil))
      (set-buffer wysihtml-current-buffer)
      (setq charset (detect-mime-charset-region (point-min)(point-max)))
      (elserv-set-result-header 
       result
       (list 'content-type (concat "text/html; charset=" (symbol-name charset))))
      (elserv-set-result-body result 
			      (encode-mime-charset-string (buffer-string) charset)))))

      



(define-minor-mode wysihtml-mode 
  "WYSIhtml minor-mode for auto-previewing of HTML through mozilla
remote interface"
  nil
  " WYSIHTML"
  nil
  (if wysihtml-mode
      (progn
	(if (eq nil wysihtml-daemon-buffer)
	    (wysihtml-start-daemon))
	(add-hook 'post-command-hook (function wysihtml-daemon-post-command)
		  nil t))
    (progn
      (remove-hook 'post-command-hook (function wysihtml-daemon-post-command)))))
	
(defun wysihtml-daemon-post-command ()
  "wysihtml that is invoked after post-command-hook"
  (if (not wysihtml-mode)
      t
    (if (local-variable-p 'wysihtml-daemon-buffer-modified-tick)
	t
      (make-local-variable 'wysihtml-daemon-buffer-modified-tick))
    (if (assoc major-mode wysihtml-active-modes-alist)
	(let* ((curval (buffer-modified-tick)))
	  (if (and (boundp 'wysihtml-daemon-buffer-modified-tick) (= curval wysihtml-daemon-buffer-modified-tick))
	      t
	    (setq wysihtml-daemon-buffer-modified-tick curval)
	    (wysihtml-daemon-moz-preview-page))))))

(defun wysihtml-daemon-moz-preview-page ()
  "Preview the current buffer in mozilla, by sending the current filename
to the daemon"
  (if (eq nil wysihtml-elserv-process)
      (wysihtml-start-elserv))
  (setq wysihtml-current-buffer (current-buffer))
  (save-excursion
    (let* ((deactivate-mark nil))
      (set-buffer wysihtml-daemon-buffer)
      (insert (concat "http://localhost:" (number-to-string wysihtml-elserv-port) "/preview\n"))
      (comint-send-input))))

(provide 'wysihtml)
