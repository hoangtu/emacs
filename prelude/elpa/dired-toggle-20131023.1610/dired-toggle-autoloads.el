;;; dired-toggle-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (dired-toggle) "dired-toggle" "dired-toggle.el"
;;;;;;  (21114 31258 477940 744000))
;;; Generated autoloads from dired-toggle.el

(autoload 'dired-toggle "dired-toggle" "\
Toggle current buffer's directory.

\(fn &optional DIR)" t nil)

;;;***

;;;### (autoloads nil nil ("dired-toggle-pkg.el") (21114 31258 584008
;;;;;;  967000))

;;;***

(provide 'dired-toggle-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; dired-toggle-autoloads.el ends here
