;;; zenburn-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads nil "zenburn-theme" "zenburn-theme.el" (21115 11507
;;;;;;  580162 924000))
;;; Generated autoloads from zenburn-theme.el

(and load-file-name (boundp 'custom-theme-load-path) (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(add-to-list 'safe-local-eval-forms '(when (require 'rainbow-mode nil t) (rainbow-mode 1)))

;;;***

;;;### (autoloads nil nil ("zenburn-theme-pkg.el") (21115 11507 649142
;;;;;;  919000))

;;;***

(provide 'zenburn-theme-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; zenburn-theme-autoloads.el ends here
