;;; fasm-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (fasm-mode) "fasm-mode" "fasm-mode.el" (21113 11892
;;;;;;  844666 860000))
;;; Generated autoloads from fasm-mode.el

(autoload 'fasm-mode "fasm-mode" "\
Major mode for editing assembly in FASM format.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("fasm-mode-pkg.el") (21113 11892 952223
;;;;;;  182000))

;;;***

(provide 'fasm-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; fasm-mode-autoloads.el ends here
