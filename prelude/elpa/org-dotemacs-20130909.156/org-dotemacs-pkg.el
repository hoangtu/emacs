(define-package "org-dotemacs" "20130909.156" "Store your emacs config as an org file, and choose which bits to load. [github]" (quote ((org "7.9.3") (cl-lib "1.0"))))
