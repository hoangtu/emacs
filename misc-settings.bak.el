;;elpa
(require 'package)
;; (add-to-list 'package-archives
;;              '("marmalade" .
;;                "http://marmalade-repo.org/packages/"))
;; (setq package-archives (cons '("tromey" . "http://tromey.com/elpa/") package-archives))
;; (package-initialize)

;;prelude
(load "~/.emacs.d/prelude/init.el")
(require 'prelude-c)
;; (require 'prelude-clojure)
;; (require 'prelude-coffee)
(require 'prelude-common-lisp)
;; (require 'prelude-css)
(require 'prelude-emacs-lisp)
(require 'prelude-erc)
(require 'prelude-erlang)
;; (require 'prelude-haskell)
(require 'prelude-js)
(require 'prelude-latex)
(require 'prelude-lisp)
;; (require 'prelude-markdown)
;; (require 'prelude-mediawiki)
(require 'prelude-org)
(require 'prelude-perl)
(require 'prelude-python)
;; (require 'prelude-ruby)
;; (require 'prelude-scala)
(require 'prelude-scheme)
;; (require 'prelude-scss)
;; (require 'prelude-web)
;; (require 'prelude-helm)
(require 'prelude-xml)
(require 'prelude-key-chord)
(setq prelude-flyspell nil)

(setq inhibit-startup-message t)
;;(byte-recompile-directory (expand-file-name "~/.emacs.d") 0)
;; By default Emacs will initiate GC every 0.76 MB allocated (gc-cons-threshold == 800000).
;; If we increase this to 20 MB (gc-cons-threshold == 20000000)
(setq gc-cons-threshold 50000000) ;; I prefere 50MB. I have plenty of RAM

;;next line add new line
;; (setq next-line-add-newlines t)

;;delete selction
;;(delete-selection-mode 1)
(setq cua-enable-cua-keys nil)           ;; don't add C-x,C-c,C-v
(cua-mode t)                             ;; for rectangles, CUA is nice

;;reindent file
;; (defun iwb ()
;;   "indent whole buffer"
;;   (interactive)
;;   (delete-trailing-whitespace)
;;   (indent-region (point-min) (point-max) nil)
;;   (untabify (point-min) (point-max)))

;;kill whole line with ctrl-k
(setq kill-whole-line t)
;;paren mode
(require 'paren)
(show-paren-mode t)
;;excaburant ctags
(setq path-to-ctags "/usr/local/bin/ctags") ;; <- your ctags path here
;;load cedet builtin config
                                        ;(load "~/.emacs.d/emacs-cedet-builtint")
(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f %s/TAGS -e -R %s" path-to-ctags dir-name (directory-file-name dir-name))))

;;undo tree
;; (load "~/.emacs.d/undo-tree")
;; (require 'undo-tree)
;; (global-undo-tree-mode)

;;cursor and mouse
(setq scroll-preserve-screen-position t) ; Scroll without moving cursor

;;turn off all bars
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))


(global-font-lock-mode t)
;;(global-set-key "\C-xs" 'save-buffer)
;; (global-set-key "\C-xv" 'quoted-insert)
;; (global-set-key "\C-xg" 'goto-line)
;;(global-set-key "\C-xf" 'search-forward)
;;(global-set-key "\C-xc" 'compile)
;; (global-set-key "\C-xt" 'text-mode);
;;(global-set-key "\C-xr" 'replace-string);
;; (global-set-key "\C-x a" 'repeat-complex-command);
;; (global-set-key "\C-x m" 'eshell);
;; (global-set-key "\C-x p" 'proced);
;;(global-set-key "\C-xw" 'what-line);
;; (global-set-key "\C-x\C-u" 'shell);
;;(global-set-key "\C-x0" 'overwrite-mode);
;; (global-set-key "\C-x\C-r" 'toggle-read-only);
;; (global-set-key "\C-t" 'kill-word);
;; (global-set-key "\C-p" 'previous-line);
;; (global-set-key "\C-u" 'backward-word);
;; (global-set-key "\C-o" 'forward-word);
;; (global-set-key "\C-h" 'backward-delete-char-untabify);
;; (global-set-key "\C-x\C-m" 'not-modified);
(setq make-backup-files 'nil);
(setq major-mode 'text-mode)
(setq text-mode-hook 'turn-on-auto-fill)
(setq auto-mode-alist (cons '("\\.cxx$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.hpp$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lzz$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.h$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.tex$" . latex-mode) auto-mode-alist))

;;(require 'font-lock)
;;(add-hook 'c-mode-hook 'turn-on-font-lock)
;;(add-hook 'c++-mode-hook 'turn-on-font-lock)

;; fullscreen
(defun toggle-fullscreen ()
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
  )
(global-set-key [(meta return)] 'toggle-fullscreen)

(when window-system  (toggle-fullscreen))

                                        ;(load "~/.emacs.d/nxhtml/autostart")
;; (setq mumamo-background-colors nil)
;; (add-to-list 'auto-mode-alist '("\\.html$" . django-html-mumamo-mode))
(global-set-key  "\C-xw"  'just-one-space)

(load "~/.emacs.d/gas-mode")

(load "~/.emacs.d/auto-pair+")

;;autopair
(autoload 'autopair-global-mode "autopair" nil t)
(autopair-global-mode)
(setq autopair-autowrap t) ;; attempt to wrap selection
(add-hook 'lisp-mode-hook
          #'(lambda () (setq autopair-dont-activate t)));
(add-hook 'ada-mode-hook
          #'(lambda () (setq autopair-dont-activate t)));
(add-hook 'sldb-mode-hook #'(lambda () (setq autopair-dont-activate t)))
(add-hook 'slime-repl-mode-hook #'(lambda () (setq autopair-dont-activate t)))
(add-hook 'python-mode-hook
          #'(lambda ()
              (push '(?' . ?')
                    (getf autopair-extra-pairs :code))
              (setq autopair-handle-action-fns
                    (list #'autopair-default-handle-action
                          #'autopair-python-triple-quote-action))))

;;auto-complete
(require 'auto-complete-config)
(ac-config-default)
(define-key ac-mode-map  [(tab)] 'auto-complete)

;; (setq-default indent-tabs-mode nil)    ; use only spaces and no tabs
;; (setq default-tab-width 4)

;; (setq python-check-command "pyflakes")
;;yasnippet
(require 'yasnippet) ;; not yasnippet-bundle
(yas-global-mode 1)
(setq ac-source-yasnippet nil)

;;line-num
(load "~/.emacs.d/linum-ex")
(require 'linum-ex)
(global-linum-mode)
(setq linum-format "%d ")

(defcustom linum-disabled-modes-list '(eshell-mode wl-summary-mode compilation-mode org-mode text-mode dired-mode doc-view-mode)

  "* List of modes disabled when global linum mode is on"
  :type '(repeat (sexp :tag "Major mode"))

  :tag " Major modes where linum is disabled: "
  :group 'linum)

(defcustom linum-disable-starred-buffers 't
  "* Disable buffers that have stars in them like *Gnu Emacs*"
  :type 'boolean
  :group 'linum)

(defun linum-on ()
  "* When linum is running globally, disable line number in modes defined in `linum-disabled-modes-list'. Changed by linum-off. Also turns off numbering in starred modes like *scratch*"
  (unless (or (minibufferp) (member major-mode linum-disabled-modes-list)

              (and linum-disable-starred-buffers (string-match "*" (buffer-name)))

              )
    (linum-mode 1)))

(provide 'setup-linum)

;;tempo-snippets
(load "~/.emacs.d/tempo-snippets")
(require 'tempo-snippets)


;;highlight-symbol
(load "~/.emacs.d/highlight-symbol")
(require 'highlight-symbol)

(global-set-key [(control f3)] 'highlight-symbol-at-point)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-prev)

;;kill-ring-search
(load "~/.emacs.d/kill-ring-search")

(autoload 'kill-ring-search "kill-ring-search"
  "Search the kill ring in the minibuffer."
  (interactive))

(global-set-key "\M-\C-y" 'kill-ring-search)

;;artist-mode
(autoload 'artist-mode "artist" "Enter artist-mode" t)

;;mini-map
(load "~/.emacs.d/minimap")
(require 'minimap)

(setq shell-prompt-pattern " ")
(setq tramp-chunksize 500)
;;sr-speedbar
(load "~/.emacs.d/sr-speedbar")
(require 'sr-speedbar)
(global-set-key (kbd "s-s") 'sr-speedbar-toggle)
;;eldoc-mode
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)
;;tramp pattern
(setq tramp-shell-prompt-pattern "^[^$>\n]*[#$%>] *\\(\[[0-9;]*[a-zA-Z] *\\)*")

;;copy X Windows clipboard
(setq x-select-enable-clipboard t)


;;switching windows
;; (defvar real-keyboard-keys
;;   '(("M-<up>"        . "\M-[1;3A")
;;     ("M-<down>"      . "\M-[1;3B")
;;     ("M-<right>"     . "\M-[1;3C")
;;     ("M-<left>"      . "\M-[1;3D")
;;     ("C-<return>"    . "\C-j")
;;     ("C-<delete>"    . "\M-[3;5~")
;;     ("C-<up>"        . "\M-[1;5A")
;;     ("C-<down>"      . "\M-[1;5B")
;;     ("C-<right>"     . "\M-[1;5C")
;;     ("C-<left>"      . "\M-[1;5D"))
;;   "An assoc list of pretty key strings
;; and their terminal equivalents.")

;; (defun key (desc)
;;   (or (and window-system (read-kbd-macro desc))
;;       (or (cdr (assoc desc real-keyboard-keys))
;;           (read-kbd-macro desc))))

;; (global-set-key  [(meta left)]'windmove-left)          ; move to left windnow
;; (global-set-key [(meta right)] 'windmove-right)        ; move to right window
;; (global-set-key  [(meta up)]'windmove-up)          ; move to left windnow
;; (global-set-key [(meta down)] 'windmove-down)        ; move to right window
;;rebind movement key
(global-set-key (kbd "M-j") 'windmove-left) ; was indent-new-comment-line
(global-set-key (kbd "M-l") 'windmove-right)  ; was downcase-word
(global-set-key (kbd "M-i") 'windmove-up) ; was tab-to-tab-stop
(global-set-key (kbd "M-k") 'windmove-down) ; was kill-sentence

;;w3m settings
(setq w3m-display-inline-image t)
(setq w3m-use-mule-ucs t)
(setq w3m-use-toolbar t)
(setq w3m-use-cookies t)

;;xcscope
(load "~/.emacs.d/xcscope")
(require 'xcscope)
(setq cscope-do-not-update-database t)
(define-key global-map [(control f3)]  'cscope-set-initial-directory)
(define-key global-map [(control f4)]  'cscope-unset-initial-directory)
(define-key global-map [(control f5)]  'cscope-find-this-symbol)
(define-key global-map [(control f6)]  'cscope-find-global-definition)
(define-key global-map [(control f7)]
  'cscope-find-global-definition-no-prompting)
(define-key global-map [(control f8)]  'cscope-pop-mark)
(define-key global-map [(control f9)]  'cscope-next-symbol)
(define-key global-map [(control f10)] 'cscope-next-file)
(define-key global-map [(control f11)] 'cscope-prev-symbol)
(define-key global-map [(control f12)] 'cscope-prev-file)
(define-key global-map [(meta f9)]  'cscope-display-buffer)
(define-key global-map [(meta f10)] 'cscope-display-buffer-toggle)

;;evil-nerd-commenter
(add-to-list 'load-path "~/.emacs.d/evil-nerd-commenter")
(evilnc-default-hotkeys)

;;bookmark-plus
;(require 'bookmark+)

;;dired
;; (define-key ctl-x-map   "d" 'diredp-dired-files)
;; (define-key ctl-x-4-map "d" 'diredp-dired-files-other-window)
(require 'dired-details)
(dired-details-install)
(require 'dired-dups)

(define-key dired-mode-map (kbd "C-s") 'dired-isearch-forward)
(define-key dired-mode-map (kbd "C-r") 'dired-isearch-backward)
(define-key dired-mode-map (kbd "ESC C-s") 'dired-isearch-forward-regexp)
(define-key dired-mode-map (kbd "ESC C-r") 'dired-isearch-backward-regexp)

(require 'dired-single)

(defun my-dired-init ()
  "Bunch of stuff to run for dired, either immediately or when it's
        loaded."
  ;; <add other stuff here>
  (define-key dired-mode-map [return] 'dired-single-buffer)
  (define-key dired-mode-map [mouse-1] 'dired-single-buffer-mouse)
  (define-key dired-mode-map "^"
    (function
     (lambda nil (interactive) (dired-single-buffer "..")))))

;; if dired's already loaded, then the keymap will be bound
(if (boundp 'dired-mode-map)
    ;; we're good to go; just add our bindings
    (my-dired-init)
  ;; it's not loaded yet, so add our bindings to the load-hook
  (add-hook 'dired-load-hook 'my-dired-init))

(global-set-key [(f5)] 'dired-single-magic-buffer)
(global-set-key [(control f5)] (function
                                (lambda nil (interactive)
                                  (dired-single-magic-buffer default-directory))))
(global-set-key [(shift f5)] (function
                              (lambda nil (interactive)
                                (message "Current directory is: %s" default-directory))))
(global-set-key [(meta f5)] 'dired-single-toggle-buffer-name)

;; allow dired to be able to delete or copy a whole dir.
(setq dired-recursive-copies (quote always)) ; “always” means no asking
(setq dired-recursive-deletes (quote top)) ; “top” means ask once

(setq dired-dwim-target t)

;; ;;pretty-mode
(require 'pretty-mode)
(global-pretty-mode 1)

;;back-button
(require 'back-button)
(back-button-mode 1)

;;recentf
;; get rid of `find-file-read-only' and replace it with something
;; more useful.
;; (global-set-key (kbd "C-x C-r") 'ido-recentf-open)

;; enable recent files mode.
;; (recentf-mode t)

                                        ; 50 files ought to be enough.
(setq recentf-max-saved-items 50)

;; (defun ido-recentf-open ()
;;   "Use `ido-completing-read' to \\[find-file] a recent file"
;;   (interactive)
;;   (if (find-file (ido-completing-read "Find recent file: " recentf-list))
;;       (message "Opening file...")
;;     (message "Aborting")))
;;visual-regex
;; (require 'visual-regexp-steroids)
;; (define-key global-map (kbd "C-c r") 'vr/replace)
;; (define-key global-map (kbd "C-c q") 'vr/query-replace)
;; to use visual-regexp's isearch instead of the built-in regexp isearch, also include the following lines:
;; (define-key global-map (kbd "C-r") 'vr/isearch-backward) ;; C-M-r
;; (define-key global-map (kbd "C-s") 'vr/isearch-forward) ;; C-M-s
;;on-screen
(on-screen-global-mode +1)
;;(fullscreen-mode-fullscreen)
;;hippie-expand
;;(global-set-key (kbd "M-/") 'hippie-expand)

;;phi-search
;; (global-set-key (kbd "C-s") 'phi-search)
;; (global-set-key (kbd "C-r") 'phi-search-backward)
;; ;;phi-search-multi-cursor
;; (defvar phi-search-from-isearch-mc/ctl-map
;;   (let ((map (make-sparse-keymap)))
;;     (define-key map (kbd ">")   'phi-search-from-isearch-mc/mark-next)
;;     (define-key map (kbd "<")   'phi-search-from-isearch-mc/mark-previous)
;;     (define-key map (kbd ". !") 'phi-search-from-isearch-mc/mark-all)
;;     map))

;; (defadvice phi-search-from-isearch-mc/setup-keys
;;   (after for-terminal activate)
;;   (define-key isearch-mode-map (kbd "C-x @ c") phi-search-from-isearch-mc/ctl-map))
;;ace-jump-mode
;; (autoload
;;   'ace-jump-mode
;;   "ace-jump-mode"
;;   "Emacs quick move minor mode"
;;   t)
;; (define-key global-map (kbd "C-c f") 'ace-jump-mode)

;; (autoload
;;   'ace-jump-mode-pop-mark
;;   "ace-jump-mode"
;;   "Ace jump back:-)"
;;   t)
;; (eval-after-load "ace-jump-mode"
;;   '(ace-jump-mode-enable-mark-sync))
;; (define-key global-map (kbd "C-c F") 'ace-jump-mode-pop-mark)
;;multiple-cursor
(require 'multiple-cursors)
(global-set-key (kbd "C-c C-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)

;;ssh-config-mode
(autoload 'ssh-config-mode "ssh-config-mode" t)
(add-to-list 'auto-mode-alist '(".ssh/config\\'"  . ssh-config-mode))
(add-to-list 'auto-mode-alist '("sshd?_config\\'" . ssh-config-mode))
(add-hook 'ssh-config-mode-hook 'turn-on-font-lock)

;;expand-region
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)
;;redo+
(require 'redo+)

;;keychord
;; (require 'key-chord)
;; (key-chord-mode 1)
;; (key-chord-define-global "xb" 'ido-switch-buffer)
;; (key-chord-define-global "zx" 'global-control-mode)
;;icicle

(icy-mode 1)
;; when using ido, the confirmation is rather annoying...
(setq confirm-nonexistent-file-or-buffer nil)

;; increase minibuffer size when ido completion is active
(add-hook 'ido-minibuffer-setup-hook
          (function
           (lambda ()
             (make-local-variable 'resize-minibuffer-window-max-height)
             (setq resize-minibuffer-window-max-height 1))))
;; ---------------------------------------
;; load elscreen
;; ---------------------------------------
(load "elscreen" "ElScreen" t)
(elscreen-start)
;; F9 creates a new elscreen, shift-F9 kills it
;;(global-set-key (kbd "C-c t a b e") 'elscreen-create)
;;(global-set-key (kbd "C-c t a b d") 'elscreen-kill)
(require 'icomplete+)
(icomplete-mode)
;;fuzzy-format
(require 'fuzzy-format)
(setq fuzzy-format-default-indent-tabs-mode nil)
(global-fuzzy-format-mode t)
;;control-mode
(control-mode-default-setup)
;;kill buffer on pane
(defun kill-next-pane ()
  "Kill the next buffer in it also."
  (interactive)
  (other-window 1)
  (kill-buffer)
  (other-window 1))

(key-chord-define-global "qw" 'kill-next-pane)
;;add smart-scan
(load "~/.emacs.d/smart-scan/smartscan")

;;browse-kill-ring
(require 'browse-kill-ring)
(global-set-key "\C-xy" 'browse-kill-ring)

;;tree-mode
(load "~/.emacs.d/tree-mode")
(require 'tree-mode)

;;windata
(load "~/.emacs.d/windata")
(require 'windata)

;;dirtree
(load "~/.emacs.d/dirtree")
(require 'dirtree)

;; ,----
;; | Create copy and cut line for Emacs
;; `----
;;copy line
;; (defun quick-copy-line ()
;;   "Copy the whole line that point is on and move to the beginning of the next line.
;;     Consecutive calls to this command append each line to the
;;     kill-ring."
;;   (interactive)
;;   (let ((beg (line-beginning-position 1))
;;         (end (line-beginning-position 2)))
;;     (if (eq last-command 'quick-copy-line)
;;         (kill-append (buffer-substring beg end) (< end beg))
;;       (kill-new (buffer-substring beg end))))
;;   (beginning-of-line 2))

;; (defun quick-cut-line ()
;;   "Cut the whole line that point is on.  Consecutive calls to this command append each line to the kill-ring."
;;   (interactive)
;;   (let ((beg (line-beginning-position 1))
;;         (end (line-beginning-position 2)))
;;     (if (eq last-command 'quick-cut-line)
;;         (kill-append (buffer-substring beg end) (< end beg))
;;       (kill-new (buffer-substring beg end)))
;;     (delete-region beg end))
;;   (beginning-of-line 1)
;;   (setq this-command 'quick-cut-line))

;; (global-set-key "\C-c\C-w" 'quick-copy-line)
;; (global-set-key "\C-c\C-k" 'quick-cut-line)

(setq-default kill-read-only-ok t)

;;buf-move
(load "~/.emacs.d/buffer-move")
(require 'windmove)
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)


;;(color-theme-tango)
;;tomorrow-theme
                                        ;(add-to-list 'load-path "~/.emacs.d/tomorrow-theme")
                                        ;(require 'color-theme-tomorrow)
                                        ;(color-theme-tomorrow-night-eighties)

(set-frame-font "Inconsolata-11")

;;save-desktop
(require 'desktop)
                                        ;(desktop-save-mode 1)
(defun my-desktop-save ()
  (interactive)
  ;; Don't call desktop-save-in-desktop-dir, as it prints a message.
  (if (eq (desktop-owner) (emacs-pid))
      (desktop-save desktop-dirname)))
(add-hook 'auto-save-hook 'my-desktop-save)

(defun emacs-process-p (pid)
  "If pid is the proess ID of an emacs process, return t, else nil.
Also returns nil if pid is nil."
  (when pid
    (let ((attributes (process-attributes pid)) (cmd))
      (dolist (attr attributes)
        (if (string= "comm" (car attr))
            (setq cmd (cdr attr))))
      (if (and cmd (or (string= "emacs" cmd) (string= "emacs.exe" cmd))) t))))

;; use only one desktop
(setq desktop-path '("~/.emacs.d/"))
(setq desktop-dirname "~/.emacs.d/")
(setq desktop-base-file-name "emacs-desktop")

;; remove desktop after it's been read
(add-hook 'desktop-after-read-hook
          '(lambda ()
             ;; desktop-remove clears desktop-dirname
             (setq desktop-dirname-tmp desktop-dirname)
             (desktop-remove)
             (setq desktop-dirname desktop-dirname-tmp)))

(defun saved-session ()
  (file-exists-p (concat desktop-dirname "/" desktop-base-file-name)))

;; use session-restore to restore the desktop manually
(defun session-restore ()
  "Restore a saved emacs session."
  (interactive)
  (if (saved-session)
      (desktop-read)
    (message "No desktop found.")))

;; use session-save to save the desktop manually
(defun session-save ()
  "Save an emacs session."
  (interactive)
  (if (saved-session)
      (if (y-or-n-p "Overwrite existing desktop? ")
          (desktop-save-in-desktop-dir)
        (message "Session not saved."))
    (desktop-save-in-desktop-dir)))

;; ask user whether to restore desktop at start-up
(add-hook 'after-init-hook
          '(lambda ()
             (if (saved-session)
                 (if (y-or-n-p "Restore desktop? ")
                     (session-restore)))))
;;shell colors
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;hungry-delete
(require 'hungry-delete)
(global-hungry-delete-mode)

;;iedit
(require 'iedit)
(require 'iedit-rect)

;;rainbow
(load "~/.emacs.d/rainbow-delimiters")
(require 'rainbow-delimiters)


;; Add hooks for modes where you want it enabled, for example:
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
(add-hook 'slime-mode-hook 'rainbow-delimiters-mode)
(when (require 'rainbow-delimiters nil 'noerror)
  (add-hook 'scheme-mode-hook 'rainbow-delimiters-mode))

(global-rainbow-delimiters-mode)


(key-chord-define-global "fg" 'beginning-of-buffer)
(key-chord-define-global "bv" 'end-of-buffer)
;; (helm-mode 1)
;;ido-mode
;; (setq ido-enable-flex-matching t)
;; (require 'flx-ido)
(setq ido-everywhere t)
(ido-mode 1)
;; (flx-ido-mode 1)
;; (setq ido-use-faces nil)
(setq ido-use-filename-at-point 'guess)
(setq ido-create-new-buffer 'always)

(setq
 ido-save-directory-list-file "~/.emacs.d/tmp/ido.last"
 ;; ido-ignore-buffers ;; ignore these guys
 ;; '("\\` " "^\*Mess" "^\*Back" ".*Completion" "^\*Ido" "^\*trace"
 ;;   "^\*compilation" "^\*GTAGS" "^session\.*" ;; "^\*"
 ;;   )
 ;;ido-work-directory-list '("~/" "~/Desktop" "~/Documents" "~src")
 ido-case-fold  t                 ; be case-insensitive
 ido-enable-last-directory-history t ; remember last used dirs
 ido-max-work-directory-list 30   ; should be enough
 ido-max-work-file-list      50   ; remember many
 ido-use-filename-at-point nil    ; don't use filename at point (annoying)
 ido-use-url-at-point nil         ; don't use url at point (annoying)
 ido-max-prospects 8              ; don't spam my minibuffer
 ;; ido-confirm-unique-completion t  ; wait for RET, even with unique completion
 )
;; (disable-theme 'zenburn)

;;saveplace
(require 'saveplace)
(setq-default save-place t)

(defalias 'yes-or-no-p 'y-or-n-p)

;;savehist
(savehist-mode 1)

;;highlight parentheses
(load "~/.emacs.d/highlight-parentheses")
(require 'highlight-parentheses)
(add-hook 'highlight-parentheses-mode-hook
          '(lambda ()
             (setq autopair-handle-action-fns
                   (append
                    (if autopair-handle-action-fns
                        autopair-handle-action-fns
                      '(autopair-default-handle-action))
                    '((lambda (action pair pos-before)
                        (hl-paren-color-update)))))))

;;auto auto indent
(set (make-local-variable 'aai-indent-function)
     'aai-indent-forward)
(add-hook 'emacs-lisp-mode-hook 'auto-auto-indent-mode)
(add-hook 'c++-mode-hook 'auto-auto-indent-mode)
(add-hook 'c-mode-hook 'auto-auto-indent-mode)
(add-hook 'lisp-mode-hook 'auto-auto-indent-mode)
(add-hook 'erlang-mode-hook 'auto-auto-indet-mode)
(add-hook 'prolog-mode-hook 'auto-auto-indent-mode)
(add-hook 'sml-mode-hook 'auto-auto-indent-mode)
;;auto indent mode
(setq auto-indent-on-visit-file t) ;; If you want auto-indent on for files
(require 'auto-indent-mode)
(auto-indent-global-mode)

;;smart-tab
(load "~/.emacs.d/smart-tab")
(require 'smarttabs)

;;fastnav
(require 'fastnav)
(global-set-key "\M-z" 'fastnav-zap-up-to-char-forward)
(global-set-key "\M-Z" 'fastnav-zap-up-to-char-backward)
(global-set-key "\M-F\M-p" 'fastnav-sprint-forward)
(global-set-key "\M-F\M-P" 'fastnav-sprint-bannckward)
;; (key-chord-define-global "f1" 'fastnav-jump-to-char-forward)
;; (key-chord-define-global "g1" 'fastnav-jump-to-char-backward)
(global-set-key "\M-F\M-M" 'fastnav-mark-to-char-forward)
(global-set-key "\M-B\M-M" 'fastnav-mark-to-char-backward)
(global-set-key "\M-F\M-I" 'fastnav-insert-at-char-forward)
(global-set-key "\M-B\M-I" 'fastnav-insert-at-char-backward)
(global-set-key "\M-F\M-R" 'fastnav-replace-char-forward)
(global-set-key "\M-B\M-R" 'fastnav-replace-char-backward)
(global-set-key "\M-F\M-D" 'fastnav-delete-char-forward)
(global-set-key "\M-B\M-D" 'fastnav-delete-char-backward)
;; (key-chord-define-global "f4" 'fastnav-execute-at-char-forward)
;; (key-chord-define-global "g4" 'fastnav-eaxecute-at-char-backward)

(require 'linum-relative)
;;move line and region
(global-set-key "\M-p"  'move-text-up)
(global-set-key "\M-n"  'move-text-down)
(eshell)
