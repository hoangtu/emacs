;;; dired-sort-menu+-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (dired-sort-dialogue) "dired-sort-menu+" "dired-sort-menu+.el"
;;;;;;  (21114 31260 441940 692000))
;;; Generated autoloads from dired-sort-menu+.el

(autoload 'dired-sort-dialogue "dired-sort-menu+" "\
A static dialogue version of the Dired sort menu.
This command *must* be run in the Dired buffer!

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("dired-sort-menu+-pkg.el") (21114 31260
;;;;;;  537138 578000))

;;;***

(provide 'dired-sort-menu+-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; dired-sort-menu+-autoloads.el ends here
