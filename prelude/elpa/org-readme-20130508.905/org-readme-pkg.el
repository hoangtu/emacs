(define-package "org-readme" "20130508.905" "Integrates Readme.org and Commentary/Change-logs. [github]"
  '((http-post-simple "1.0")
    (yaoddmuse "0.1.1")
    (header2 "21.0")
    (lib-requires "21.0")))

