;;; face-remap+-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (text-scale-increase text-scale-resize-window)
;;;;;;  "face-remap+" "face-remap+.el" (21114 41089 197683 53000))
;;; Generated autoloads from face-remap+.el

(defvar text-scale-resize-window t "\
*Non-nil means text scaling resizes the window or frame accordingly.
For example, if you use `C-x C--' (`text-scale-decrease')' to make the
text smaller, then the window or frame is made smaller by a similar
factor.

If the window is not alone in its frame, then the window is resized.
Otherwise, the frame is resized (provided you also use library
`fit-frame.el').  The frame is always resized both horizontally and
vertically.")

(custom-autoload 'text-scale-resize-window "face-remap+" t)

(autoload 'text-scale-increase "face-remap+" "\
Increase the height of the default face in the current buffer by INC steps.
If the new height is other than the default, `text-scale-mode' is enabled.

Each step scales the height of the default face by the variable
`text-scale-mode-step' (a negative number of steps decreases the
height by the same amount).  As a special case, an argument of 0
removes any scaling currently active.

If option `text-scale-resize-window' is non-nil, then the selected
window or frame is resized accordingly, so as to keep roughly the same
text visible in the window.  Normally, it is the window that is
resized, but if the window is alone in its frame (and if you use
library `fit-frame.el'), then the frame is resized instead.

See option `text-scale-resize-window' for the possible behaviors.

\(fn INC)" t nil)

;;;***

;;;### (autoloads nil nil ("face-remap+-pkg.el") (21114 41089 303942
;;;;;;  108000))

;;;***

(provide 'face-remap+-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; face-remap+-autoloads.el ends here
