(setq inhibit-startup-message t)
;;(byte-recompile-directory (expand-file-name "~/.emacs.d") 0)

;;move-line-region
(load-file "~/.emacs.d/move-line-and-region.el")
(defun move-line-region-up (start end n)
  (interactive "r\np")
  (if (region-active-p) (move-region-up start end n) (move-line-up n)))

(defun move-line-region-down (start end n)
  (interactive "r\np")
  (if (region-active-p) (move-region-down start end n) (move-line-down n)))

(global-set-key (kbd "M-p") 'move-line-region-up)
(global-set-key (kbd "M-n") 'move-line-region-down)

;;reindent file
(defun iwb ()
  "indent whole buffer"
  (interactive)
  (delete-trailing-whitespace)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max)))
;;cc-mode linux style
(setq c-default-style "linux"
      c-basic-offset 4)
;;smart-tab
(load-file "~/.emacs.d/smart-tab.el")
(require 'smarttabs)
;;kill whole line with ctrl-k
(setq kill-whole-line t)
;;paren mode
(require 'paren)
(show-paren-mode t)
                                        ;excaburant ctags
(setq path-to-ctags "/usr/local/bin/ctags") ;; <- your ctags path here
;;cedet
(load-file "~/.emacs.d/emacs-rc-cedet.el")
(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -f %s/TAGS -e -R %s" path-to-ctags dir-name (directory-file-name dir-name)))
  )

(load-file "~/.emacs.d/undo-tree.el")
(require 'undo-tree)
(global-undo-tree-mode)


;;turn off all bars
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))

(global-font-lock-mode t)
                                        ;(global-set-key "\C-xs" 'save-buffer)
(global-set-key "\C-xv" 'quoted-insert)
(global-set-key "\C-xg" 'goto-line)
                                        ;(global-set-key "\C-xf" 'search-forward)
                                        ;(global-set-key "\C-xc" 'compile)
(global-set-key "\C-xt" 'text-mode);
                                        ;(global-set-key "\C-xr" 'replace-string);
(global-set-key "\C-xa" 'repeat-complex-command);
(global-set-key "\C-xm" 'manual-entry);
                                        ;(global-set-key "\C-xw" 'what-line);
(global-set-key "\C-x\C-u" 'shell);
                                        ;(global-set-key "\C-x0" 'overwrite-mode);
(global-set-key "\C-x\C-r" 'toggle-read-only);
                                        ;(global-set-key "\C-t" 'kill-word);
                                        ;(global-set-key "\C-p" 'previous-line);
                                        ;(global-set-key "\C-u" 'backward-word);
                                        ;(global-set-key "\C-o" 'forward-word);
;;(global-set-key "\C-h" 'backward-delete-char-untabify);
(global-set-key "\C-x\C-m" 'not-modified);
(setq make-backup-files 'nil);
(setq default-major-mode 'text-mode)
(setq text-mode-hook 'turn-on-auto-fill)
(set-default-font "-misc-fixed-medium-r-normal--15-140-*-*-c-*-*-1")
(setq auto-mode-alist (cons '("\\.cxx$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.hpp$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.lzz$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.h$" . c++-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.tex$" . latex-mode) auto-mode-alist))

                                        ;(require 'font-lock)
                                        ;(add-hook 'c-mode-hook 'turn-on-font-lock)
                                        ;(add-hook 'c++-mode-hook 'turn-on-font-lock)

(defun toggle-fullscreen ()
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0))
  )
(global-set-key [(meta return)] 'toggle-fullscreen)

(when window-system  (toggle-fullscreen))

                                        ;(load "~/.emacs.d/nxhtml/autostart.el")
(setq mumamo-background-colors nil)
(add-to-list 'auto-mode-alist '("\\.html$" . django-html-mumamo-mode))

(add-to-list 'load-path "~/.emacs.d/autopair")

(load "~/.emacs.d/auto-pair+.el")
(load "~/.emacs.d/gas-mode.el")

;;autopair
(autoload 'autopair-global-mode "autopair" nil t)
(autopair-global-mode)
(add-hook 'lisp-mode-hook
          #'(lambda () (setq autopair-dont-activate t)));
(add-hook 'ada-mode-hook
          #'(lambda () (setq autopair-dont-activate t)));
(add-hook 'sldb-mode-hook #'(lambda () (setq autopair-dont-activate t)))
(add-hook 'python-mode-hook
          #'(lambda ()
              (push '(?' . ?')
                    (getf autopair-extra-pairs :code))
              (setq autopair-handle-action-fns
                    (list #'autopair-default-handle-action
                          #'autopair-python-triple-quote-action))))

;;auto-complete
(add-to-list 'load-path "~/.emacs.d/auto-complete-1.3.1/")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/auto-complete-1.3.1/dict")
(ac-config-default)

(setq-default indent-tabs-mode nil)    ; use only spaces and no tabs
(setq default-tab-width 4)

(setq python-check-command "pyflakes")
;;yasnippet
(add-to-list 'load-path
             "~/.emacs.d/yasnippet")
(require 'yasnippet) ;; not yasnippet-bundle
(yas-global-mode 1)

(add-to-list 'load-path "~/.emacs.d/plugins/anything-config")
(require 'anything-config)

;; (global-set-key (kbd "C-x b")
;;                 (lambda() (interactive)
;;                   (anything
;;                    :prompt "Switch to: "
;;                    :candidate-number-limit 10                 ;; up to 10 of each
;;                    :sources
;;                    '( anything-c-source-buffers               ;; buffers
;;                       anything-c-source-recentf               ;; recent files
;;                       anything-c-source-bookmarks             ;; bookmarks
;;                       anything-c-source-files-in-current-dir+ ;; current dir
;;                       anything-c-source-locate))))            ;; use 'locate'

(global-set-key (kbd "C-c I")  ;; i -> info
                (lambda () (interactive)
                  (anything
                   :prompt "Info about: "
                   :candidate-number-limit 3
                   :sources
                   '( anything-c-source-info-libc             ;; glibc docs
                      anything-c-source-man-pages             ;; man pages
                      anything-c-source-info-emacs))))        ;; emacs

(add-hook 'emacs-lisp-mode-hook
          (lambda()
            ;; other stuff...
            ;; ...
            ;; put useful info under C-c i
            (local-set-key (kbd "C-c i")
                           (lambda() (interactive)
                             (anything
                              :prompt "Info about: "
                              :candidate-number-limit 5
                              :sources
                              '( anything-c-source-emacs-functions
                                 anything-c-source-emacs-variables
                                 anything-c-source-info-elisp
                                 anything-c-source-emacs-commands
                                 anything-c-source-emacs-source-defun
                                 anything-c-source-emacs-lisp-expectations
                                 anything-c-source-emacs-lisp-toplevels
                                 anything-c-source-emacs-functions-with-abbrevs
                                 anything-c-source-info-emacs))))))

(defun my-anything ()
  (interactive)
  (anything-other-buffer
   '(anything-c-source-buffers
     anything-c-source-file-name-history
     anything-c-source-info-pages
     anything-c-source-info-elisp
     anything-c-source-man-pages
     anything-c-source-locate
     anything-c-source-emacs-commands)
   " *my-anything*"))


;;auto-indent
(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (and (not current-prefix-arg)
                (member major-mode '(emacs-lisp-mode lisp-mode
                                                     clojure-mode    scheme-mode
                                                     haskell-mode    ruby-mode
                                                     rspec-mode      python-mode
                                                     c-mode          c++-mode
                                                     objc-mode       latex-mode
                                                     plain-tex-mode))
                (let ((mark-even-if-inactive transient-mark-mode))
                  (indent-region (region-beginning) (region-end) nil))))))
(define-key global-map (kbd "RET") 'newline-and-indent)
                                        ; flymake
(add-to-list 'load-path "~/.emacs.d/flymake")
(load "~/.emacs.d/fringe-helper.el")
;; (load "~/.emacs.d/flymake-extension.el")
;; (load "~/.emacs.d/indicators.el")

(require 'flymake)
                                        ;(require 'flymake-extension)
(require 'flymake-cursor)
(require 'fringe-helper)
(require 'flymake-shell)
;; (require 'indicator)

(require 'rfringe)
(add-hook 'sh-mode-hook 'flymake-shell-load)

(setq flymake-max-parallel-syntax-checks nil)
(setq flymake-run-in-place nil)
(setq flymake-number-of-errors-to-display nil)


(defvar flymake-fringe-overlays nil)
(make-variable-buffer-local 'flymake-fringe-overlays)

(defadvice flymake-make-overlay (after add-to-fringe first
                                       (beg end tooltip-text face mouse-face)
                                       activate compile)
  (push (fringe-helper-insert-region
         beg end
         (fringe-lib-load (if (eq face 'flymake-errline)
                              fringe-lib-exclamation-mark
                            fringe-lib-question-mark))
         'left-fringe 'font-lock-warning-face)
        flymake-fringe-overlays))

(defadvice flymake-delete-own-overlays (after remove-from-fringe activate
                                              compile)
  (mapc 'fringe-helper-remove flymake-fringe-overlays)
  (setq flymake-fringe-overlays nil))
;;switch between c/c++ source and header file
(defun dts-switch-between-header-and-source ()
  "Switch between a c/c++ header (.h) and its corresponding source (.c/.cpp)."
  (interactive)
  ;; grab the base of the current buffer's file name
  (setq bse (file-name-sans-extension buffer-file-name))
  ;; and the extension, converted to lowercase so we can
  ;; compare it to "h", "c", "cpp", etc
  (setq ext (downcase (file-name-extension buffer-file-name)))
  ;; This is like a c/c++ switch statement, except that the
  ;; conditions can be any true/false evaluated statement
  (cond
   ;; first condition - the extension is "h"
   ((equal ext "h")
    ;; first, look for bse.c
    (setq nfn (concat bse ".c"))
    (if (file-exists-p nfn)
        ;; if it exists, either switch to an already-open
        ;;  buffer containing that file, or open it in a new buffer
        (find-file nfn)
      ;; this is the "else" part - note that if it is more than
      ;;  one line, it needs to be wrapped in a (progn )
      (progn
        ;; look for a bse.cpp
        (setq nfn (concat bse ".cpp"))
        ;; likewise
        (find-file nfn)
        )
      )
    )
   ;; second condition - the extension is "c" or "cpp"
   ((or (equal ext "cpp") (equal ext "c"))
    ;; look for a corresponding bse.h
    (setq nfn (concat bse ".h"))
    (find-file nfn)
    )
   )
  )
(global-set-key (kbd "C-c s") 'dts-switch-between-header-and-source)
;;ecb
                                        ;(add-to-list 'load-path "~/.emacs.d/ecb-2.40")
                                        ;(require 'ecb)
                                        ;(require 'ecb-autoloads)
                                        ;(setq ecb-tip-of-the-day nil)
                                        ;(ecb-activate)
                                        ;org-mode
                                        ;(setq load-path (cons "~/Programs/org-mode" load-path))
                                        ;(setq load-path (cons "~/Programs/org-mode/contrib/lisp" load-path))
                                        ;(require 'org-install)
                                        ;(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
                                        ;(define-key global-map "\C-cl" 'org-store-link)
                                        ;(define-key global-map "\C-ca" 'org-agenda)
                                        ;(setq org-log-done t)
                                        ;(global-set-key "\C-cl" 'org-store-link)
                                        ;(global-set-key "\C-cc" 'org-capture)
                                        ;(global-set-key "\C-ca" 'org-agenda)
                                        ;(global-set-key "\C-cb" 'org-iswitchb)


;;line-num
(load "~/.emacs.d/linum-ex.el")
(require 'linum-ex)
(global-linum-mode)
(setq linum-format "%d ")

;;gas-mode
(add-to-list 'load-path "~/.emacs.d/gas-mode.el")

(add-to-list 'load-path "~/.emacs.d/myplan.el")

;;tempo-snippets
(load "~/.emacs.d/tempo-snippets.el")
(require 'tempo-snippets)


;;highlight-symbol
(load "~/.emacs.d/highlight-symbol.el")
(require 'highlight-symbol)

(global-set-key [(control f3)] 'highlight-symbol-at-point)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-prev)

;;kill-ring-search
(load "~/.emacs.d/kill-ring-search.el")

(autoload 'kill-ring-search "kill-ring-search"
  "Search the kill ring in the minibuffer."
  (interactive))

(global-set-key "\M-\C-y" 'kill-ring-search)

;;artist-mode
(autoload 'artist-mode "artist" "Enter artist-mode" t)

;;rainbow
(load "~/.emacs.d/rainbow-delimiters.el")
(require 'rainbow-delimiters)
;; Add hooks for modes where you want it enabled, for example:
(add-hook 'clojure-mode-hook 'rainbow-delimiters-mode)
(add-hook 'slime-mode-hook 'rainbow-delimiters-mode)
(when (require 'rainbow-delimiters nil 'noerror)
  (add-hook 'scheme-mode-hook 'rainbow-delimiters-mode))


;;ac-dabbrev
;;(load "~/.emacs.d/dabbrev-ac.el")

;;mini-map
(load-file "~/.emacs.d/minimap.el")
(require 'minimap)

(setq shell-prompt-pattern " ")
(setq tramp-chunksize 500)
;;sr-speedbar
(load "~/.emacs.d/sr-speedbar.el")
(require 'sr-speedbar)
(global-set-key (kbd "s-s") 'sr-speedbar-toggle)
;;eldoc-mode
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)
;;tramp pattern
(setq tramp-shell-prompt-pattern "^[^$>\n]*[#$%>] *\\(\[[0-9;]*[a-zA-Z] *\\)*")

(eshell)
;;copy X Windows clipboard
(setq x-select-enable-clipboard t)
;;cscope
(load "~/.emacs.d/xcscope.el")
(require 'xcscope)
(setq cscope-do-not-update-database t)

;;switching windows
(defvar real-keyboard-keys
  '(("M-<up>"        . "\M-[1;3A")
    ("M-<down>"      . "\M-[1;3B")
    ("M-<right>"     . "\M-[1;3C")
    ("M-<left>"      . "\M-[1;3D")
    ("C-<return>"    . "\C-j")
    ("C-<delete>"    . "\M-[3;5~")
    ("C-<up>"        . "\M-[1;5A")
    ("C-<down>"      . "\M-[1;5B")
    ("C-<right>"     . "\M-[1;5C")
    ("C-<left>"      . "\M-[1;5D"))
  "An assoc list of pretty key strings
and their terminal equivalents.")

(defun key (desc)
  (or (and window-system (read-kbd-macro desc))
      (or (cdr (assoc desc real-keyboard-keys))
          (read-kbd-macro desc))))

(global-set-key  [(meta left)]'windmove-left)          ; move to left windnow
(global-set-key [(meta right)] 'windmove-right)        ; move to right window
(global-set-key  [(meta up)]'windmove-up)          ; move to left windnow
(global-set-key [(meta down)] 'windmove-down)        ; move to right window
                                        ;(global-set-key (key "M-<up>") 'windmove-up)              ; move to upper window
                                        ;(global-set-key (key "M-<down>") 'windmove-down)          ; move to downer window
                                        ;(toggle-fullscreen)

(setq w3m-display-inline-image t)
(setq w3m-use-mule-ucs t)
(setq w3m-use-toolbar t)
(setq w3m-use-cookies t)

;;flex-mode
(load "~/.emacs.d/flex-mode.el")
(setq auto-mode-alist (cons '("\\.flex$" . flex-mode) auto-mode-alist))
(setq auto-mode-alist (cons '("\\.y$" . c++-mode) auto-mode-alist))

;;ido-mode
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
(setq ido-use-filename-at-point 'guess)
(setq ido-create-new-buffer 'always)
;;xcscope
(load "~/.emacs.d/xcscope.el")
(require 'xcscope)
(define-key global-map [(control f3)]  'cscope-set-initial-directory)
(define-key global-map [(control f4)]  'cscope-unset-initial-directory)
(define-key global-map [(control f5)]  'cscope-find-this-symbol)
(define-key global-map [(control f6)]  'cscope-find-global-definition)
(define-key global-map [(control f7)]
  'cscope-find-global-definition-no-prompting)
(define-key global-map [(control f8)]  'cscope-pop-mark)
(define-key global-map [(control f9)]  'cscope-next-symbol)
(define-key global-map [(control f10)] 'cscope-next-file)
(define-key global-map [(control f11)] 'cscope-prev-symbol)
(define-key global-map [(control f12)] 'cscope-prev-file)
(define-key global-map [(meta f9)]  'cscope-display-buffer)
(define-key global-map [(meta f10)] 'cscope-display-buffer-toggle)
;;slime
(setq inferior-lisp-program "/usr/local/bin/sbcl") ; your Lisp system
(add-to-list 'load-path "~/.emacs.d/slime/")  ; your SLIME directory
(add-to-list 'load-path "~/.emacs.d/slime/contrib")  ; your SLIME directory

(eval-after-load "slime"
  '(progn
     (setq slime-lisp-implementations
           '((sbcl ("/usr/bin/sbcl"))
             (ecl ("/usr/bin/ecl"))
             (clisp ("/usr/bin/clisp"))))
     (slime-setup '(
                    slime-asdf
                    slime-autodoc
                    slime-editing-commands
                    slime-fancy-inspector
                    slime-fontifying-fu
                    slime-fuzzy
                    slime-indentation
                    slime-mdot-fu
                    slime-package-fu
                    slime-references
                    slime-repl
                    slime-sbcl-exts
                    slime-scratch
                    slime-xref-browser
                    ))
     (slime-autodoc-mode)
     (setq slime-complete-symbol*-fancy t)
     (setq slime-complete-symbol-function
           'slime-fuzzy-complete-symbol)))

(require 'slime)
(slime-setup)
;;par-edit
(load "~/.emacs.d/paredit.el")
(autoload 'paredit-mode "paredit"
  "Minor mode for pseudo-structurally editing Lisp code." t)
(add-hook 'emacs-lisp-mode-hook       (lambda () (paredit-mode +1)))
(add-hook 'lisp-mode-hook             (lambda () (paredit-mode +1)))
(add-hook 'lisp-interaction-mode-hook (lambda () (paredit-mode +1)))
(add-hook 'scheme-mode-hook           (lambda () (paredit-mode +1)))
(add-hook 'ada-mode-hook              (lambda () (paredit-mode +1)))

(defadvice paredit-mode (around disable-autopairs-around (arg))
  "Disable autopairs mode if paredit-mode is turned on"
  ad-do-it
  (if (null ad-return-value)
      (autopair-mode 1)
    (autopair-mode 0)))

(ad-activate 'paredit-mode)

;; Erlang-mode
;; (setq load-path (cons  "/usr/local/lib/erlang/lib/tools-2.6.8/emacs/"
;;      load-path))
;;      (setq erlang-root-dir "/usr/local/lib/erlang")
;;      (setq exec-path (cons "/usr/local/lib/erlang/bin" exec-path))
;;      (require 'erlang-start)

;; Distel
;; (push "~/.emacs.d/distel/elisp/" load-path)
;; (require 'distel)
;; (distel-setup)
;;clange-complete
(load "~/.emacs.d/auto-complete-clang-config.el")
(load "~/.emacs.d/clang-completion-mode.el")
(load "~/.emacs.d/custom-c.el")
;;prolog-mode
(load "~/.emacs.d/prolog.el")
(setq prolog-program-name "~/BProlog/bp")
(autoload 'run-prolog "prolog" "Start a Prolog sub-process." t)
(autoload 'prolog-mode "prolog" "Major mode for editing Prolog programs." t)
(autoload 'mercury-mode "prolog" "Major mode for editing Mercury programs." t)
(setq prolog-system 'swi)  ; optional, the system you are using;
                                        ; see `prolog-system' below for possible values
(setq auto-mode-alist (append '(("\\.pl$" . prolog-mode)
                                        ;("\\.m$" . mercury-mode)
                                )
                              auto-mode-alist))

(setq prolog-indent-width 4
      ;;             prolog-electric-dot-flag t
      ;;             prolog-electric-dash-flag t
      prolog-electric-colon-flag t)
(setq prolog-system 'swi
      prolog-program-switches '((swi ("-G128M" "-T128M" "-L128M" "-O"))
                                (t nil))
      prolog-electric-if-then-else-flag t)


;;prolog-flymake
(add-hook 'prolog-mode-hook
          (lambda ()
            (require 'flymake)
            (make-local-variable 'flymake-allowed-file-name-masks)
            (make-local-variable 'flymake-err-line-patterns)
            (setq flymake-err-line-patterns
                  '(("ERROR: (?\\(.*?\\):\\([0-9]+\\)" 1 2)
                    ("Warning: (\\(.*\\):\\([0-9]+\\)" 1 2)))
            (setq flymake-allowed-file-name-masks
                  '(("\\.pl\\'" flymake-prolog-init)))
            (flymake-mode 1)))

(defun flymake-prolog-init ()
  (let* ((temp-file   (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
         (local-file  (file-relative-name
                       temp-file
                       (file-name-directory buffer-file-name))))
    (list "prolog" (list "-q" "-t" "halt" "-s " local-file))))
;;iedit
(add-to-list 'load-path "~/.emacs.d/iedit")
(require 'iedit)
(require 'iedit-rect)

;;el-get
(require 'package)
(setq package-archives (cons '("tromey" . "http://tromey.com/elpa/") package-archives))
(package-initialize)

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")

(unless (require 'el-get nil 'noerror)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (let (el-get-master-branch)
      (goto-char (point-max))
      (eval-print-last-sexp))))

(el-get 'sync)

(add-to-list 'load-path "~/.emacs.d/el-get/el-get")
(require 'el-get)

;;ruby-mode
(defun ruby-mode-hook ()
  (autoload 'ruby-mode "ruby-mode" nil t)
  (add-to-list 'auto-mode-alist '("Capfile" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Gemfile" . ruby-mode))
  (add-to-list 'auto-mode-alist '("Rakefile" . ruby-mode))
  (add-to-list 'auto-mode-alist '("\\.rake\\'" . ruby-mode))
  (add-to-list 'auto-mode-alist '("\\.rb\\'" . ruby-mode))
  (add-to-list 'auto-mode-alist '("\\.ru\\'" . ruby-mode))
  (add-hook 'ruby-mode-hook '(lambda ()
                               (setq ruby-deep-arglist t)
                               (setq ruby-deep-indent-paren nil)
                               (setq c-tab-always-indent nil)
                               (require 'inf-ruby)
                               (require 'ruby-compilation))))
(defun rhtml-mode-hook ()
  (autoload 'rhtml-mode "rhtml-mode" nil t)
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . rhtml-mode))
  (add-to-list 'auto-mode-alist '("\\.rjs\\'" . rhtml-mode))
  (add-hook 'rhtml-mode '(lambda ()
                           (define-key rhtml-mode-map (kbd "M-s") 'save-buffer))))
(defun yaml-mode-hook ()
  (autoload 'yaml-mode "yaml-mode" nil t)
  (add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
  (add-to-list 'auto-mode-alist '("\\.yaml$" . yaml-mode)))
(defun css-mode-hook ()
  (autoload 'css-mode "css-mode" nil t)
  (add-hook 'css-mode-hook '(lambda ()
                              (setq css-indent-level 2)
                              (setq css-indent-offset 2))))

(eval-after-load 'ruby-mode
  '(progn
     (define-key ruby-mode-map (kbd "RET") 'reindent-then-newline-and-indent)
     (define-key ruby-mode-map (kbd "TAB") 'indent-for-tab-command)))

(setq el-get-sources
      '((:name ruby-mode
               :type elpa
               :load "ruby-mode.el"
               :after (progn (ruby-mode-hook)))
        (:name inf-ruby  :type elpa)
        (:name ruby-compilation :type elpa)
        (:name css-mode
               :type elpa
               :after (progn (css-mode-hook)))
        (:name textmate
               :type git
               :url "git://github.com/defunkt/textmate.el"
               :load "textmate.el")
        (:name rvm
               :type git
               :url "http://github.com/djwhitt/rvm.el.git"
               :load "rvm.el"
               :compile ("rvm.el")
               :after (progn (rvm-use-default)))
        (:name rhtml
               :type git
               :url "https://github.com/eschulte/rhtml.git"
               :features rhtml-mode
               :after (progn (rhtml-mode-hook)))
        (:name yaml-mode
               :type git
               :url "http://github.com/yoshiki/yaml-mode.git"
               :features yaml-mode
               :after (progn (yaml-mode-hook)))))

;;wombat theme
                                        ;(load-theme 'wombat t)

;;ess
                                        ;(require 'ess-site)
;;octave
(autoload 'octave-mode "octave-mode" nil t)
(setq auto-mode-alist
      (cons '("\\.m$" . octave-mode) auto-mode-alist))
(add-hook 'octave-mode-hook
          (lambda ()
            (abbrev-mode 1)
            (auto-fill-mode 1)
            (if (eq window-system 'x)
                (font-lock-mode 1))))
;;bookmark-plus
(add-to-list 'load-path "~/.emacs.d/bookmark-plus")
(require 'bookmark+)

;;ada-mode
(add-to-list 'load-path "~/.emacs.d/ada-mode")
(require 'ada-mode)

;; Flymake for Ada
(require 'flymake)
(defun flymake-ada-init ()
  (flymake-simple-make-init-impl
   'flymake-create-temp-with-folder-structure nil nil
   buffer-file-name
   'flymake-get-ada-cmdline))

(defun flymake-get-ada-cmdline (source base-dir)
  `("gnatmake" ("-gnatc" "-gnatwa" ,(concat "-I" (expand-file-name base-dir)) ,source)))

(push '(".+\\.adb$" flymake-ada-init) flymake-allowed-file-name-masks)
(push '(".+\\.ads$" flymake-ada-init) flymake-allowed-file-name-masks)

(push '("\\([^:]*\\):\\([0-9]+\\):[0-9]+: \\(.*\\)"
        1 2 nil 3)
      flymake-err-line-patterns)

                                        ;else-mode
                                        ;(load-file "~/.emacs.d/emacs-else/else-mode.el")
                                        ;(require 'else-mode)
                                        ;asm86-mode

;; Import flymake
(require 'flymake)

;; Define function
(defun flymake-cc-init ()
  (let* (;; Create temp file which is copy of current file
         (temp-file   (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
         ;; Get relative path of temp file from current directory
         (local-file  (file-relative-name
                       temp-file
                       (file-name-directory buffer-file-name))))

    ;; Construct compile command which is defined list.
    ;; First element is program name, "g++" in this case.
    ;; Second element is list of options.
    ;; So this means "g++ -Wall -Wextra -fsyntax-only tempfile-path"
    (list "g++" (list "-std=c++11" "-Wall" "-Wextra" "-fsyntax-only" local-file))))

;; Enable above flymake setting for C++ files(suffix is '.cpp')
(push '("\\.cpp$" flymake-cc-init) flymake-allowed-file-name-masks)

;; Enable flymake-mode for C++ files.
(add-hook 'c++-mode-hook 'flymake-mode)

;;asmMODE
(autoload 'asm86-mode "~/.emacs.d/asm86-mode.el")
                                        ;(require 'asm86-mode)

(setq auto-mode-alist
      (append '(("\\.asm\\'" . asm86-mode) ("\\.inc\\'" . asm86-mode)) auto-mode-alist))
(add-hook 'asm86-mode-hook 'turn-on-font-lock)
(add-hook 'asm86-mode-hook
          '(lambda ()
             (define-key asm86-mode-map "\r" 'reindent-then-newline-and-indent)))
;;buf-move
(load "~/.emacs.d/buffer-move.el")
(require 'windmove)
(global-set-key (kbd "<C-S-up>")     'buf-move-up)
(global-set-key (kbd "<C-S-down>")   'buf-move-down)
(global-set-key (kbd "<C-S-left>")   'buf-move-left)
(global-set-key (kbd "<C-S-right>")  'buf-move-right)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(global-semantic-tag-folding-mode t nil (semantic-util-modes))
 '(semantic-idle-scheduler-idle-time 3)
 '(semantic-self-insert-show-completion-function (lambda nil (semantic-ia-complete-symbol-menu (point)))))
;;color-theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/color-theme-6.6.0")
(add-to-list 'load-path "~/.emacs.d/color-theme-solarized")
(require 'color-theme)
(color-theme-initialize)
(require 'color-theme-solarized)
                                        ;(load-theme 'solarized-light t)

                                        ;(load-theme 'zenburn t)
(load "~/.emacs.d/themes/color-theme-djcb-dark.el")
(load "~/.emacs.d/themes/color-theme-gentooish.el")
(load "~/.emacs.d/themes/color-theme-tango.el")
(color-theme-tango)
(set-default-font "Inconsolata-11")
;;save-desktop
(require 'desktop)
(desktop-save-mode 1)
(defun my-desktop-save ()
  (interactive)
  ;; Don't call desktop-save-in-desktop-dir, as it prints a message.
  (if (eq (desktop-owner) (emacs-pid))
      (desktop-save desktop-dirname)))
(add-hook 'auto-save-hook 'my-desktop-save)
;;shell colors
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
;;emacs-ctable
(load "~/.emacs.d/ctable.el")
(require 'ctable)
;;emacs deferred
(add-to-list 'load-path "~/.emacs.d/emacs-deferred")
;;emacs-epc
(add-to-list 'load-path "~/.emacs.d/emacs-epc")
(require 'epc)
;;jedi-mode
(add-to-list 'load-path "~/.emacs.d/emacs-jedi")
(autoload 'jedi:setup "jedi" nil t)
(add-hook 'python-mode-hook 'auto-complete-mode)
(add-hook 'python-mode-hook 'jedi:setup)
;(add-hook 'python-mode-hook 'electric-pair-mode)
(setq jedi:setup-keys t)                      ; optional
(setq jedi:complete-on-dot t)                 ; optional
;; (eval-after-load "python"
;;   '(define-key python-mode-map "\C-cx" 'jedi-direx:pop-to-buffer))
;;fly-check
(add-hook 'after-init-hook #'global-flycheck-mode)
;;smartparens
;(load "~/.emacs.d/smartpar-conf.el")

;;flycheck-color-mode

(add-hook 'flycheck-mode-hook 'flycheck-color-mode-line-mode)

(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)


(setq autopair-autowrap t) ;; attempt to wrap selection
(eval-after-load "python"
  '(define-key python-mode-map "C-m" 'newline-and-indent))
(eval-after-load "python"
  '(define-key python-mode-map "M-e" 'forward-sentence))
(global-set-key  "\C-xw"  'just-one-space)

(add-to-list 'load-path "~/.emacs.d/helm")
(require 'helm-config)
(helm-mode 1)
