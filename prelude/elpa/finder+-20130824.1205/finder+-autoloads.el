;;; finder+-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (finder-commentary) "finder+" "finder+.el" (21115
;;;;;;  54999 739076 915000))
;;; Generated autoloads from finder+.el

(autoload 'finder-commentary "finder+" "\
Display FILE's commentary section.
FILE should be in a form suitable for passing to `locate-library'.

\(fn FILE)" t nil)

;;;***

;;;### (autoloads nil nil ("finder+-pkg.el") (21115 54999 864025
;;;;;;  488000))

;;;***

(provide 'finder+-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; finder+-autoloads.el ends here
