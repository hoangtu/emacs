# Generated automatically from Makefile.in by configure.
## This is a -*- makefile -*-

# What the Emacs 19 binary is called on your system
EMACS = emacs

# Prefix for constructing installation directory paths
prefix = /usr/local
exec_prefix = $(prefix)

# Shared directory for read-only data files
datadir = $(prefix)/share

# Where to put the .el and .elc files
lispdir=$(datadir)/emacs/site-lisp

# Installation command
INSTALL = /usr/bin/install -c
INSTALL_DATA = ${INSTALL} -m 644

# Various auxiliary programs
TAR=tar

srcdir = .

SOURCES = artist.el
OBJECTS = artist.elc

DISTFILES = $(SOURCES) COPYING ChangeLog INSTALL Makefile.in \
	README TODO BUGS configure configure.in install-sh load-path.hack \
	mkinstalldirs

SHELL = /bin/sh
#.PHONY: all clean dist distclean install \
#	installdirs ps uninstall
.SUFFIXES:
.SUFFIXES: .elc .el

.el.elc:
	$(EMACS) -batch -l $(srcdir)/load-path.hack \
	  -f batch-byte-compile $<

all: $(OBJECTS)

install: all installdirs
	for f in $(SOURCES); do \
	  $(INSTALL_DATA) $(srcdir)/$$f $(lispdir); \
	done;
	for f in $(OBJECTS); do \
	  $(INSTALL_DATA) $$f $(lispdir); \
	done;

# Make sure all installation directories actually exist
# by making them if necessary.
installdirs: mkinstalldirs
	$(srcdir)/mkinstalldirs $(lispdir)

uninstall:
	-cd $(lispdir) && rm -f $(SOURCES) $(OBJECTS)

TAGS: $(SOURCES)
	cd $(srcdir) && etags $(SOURCES)

clean:
	rm -f $(OBJECTS)
	-rm -f *.aux *.cp *.cps *.fn *.ky *.log *.pg *.toc *.tp *.vr
	-rm -f *.html
	rm -f TAGS

distclean: clean
	-rm -f *~ *.tar.gz
	rm -f Makefile config.status config.cache config.log

${srcdir}/configure: configure.in
	cd ${srcdir} && autoconf
     
Makefile: Makefile.in config.status
	./config.status
     
config.status: ${srcdir}/configure
	./config.status --recheck

dist: $(DISTFILES)
	version=`perl -ne 'print $$1 if /defconst artist-version \"(.*)\"/' \
		 artist.el`; \
	distname=artist-$$version; \
	rm -rf $$distname; \
	mkdir $$distname; \
	for file in $(DISTFILES); do \
	  ln $$file $$distname/$$file; \
	done; \
	$(TAR) -chz -f $$distname.tar.gz $$distname; \
	rm -rf $$distname
